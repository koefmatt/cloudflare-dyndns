from CloudFlare import CloudFlare, exceptions
from werkzeug.exceptions import HTTPException
from flask import Flask, send_from_directory, jsonify, request
import ipaddress
import sys
import os


def bitwise_and_ipv6(addr1, addr2):
    result_int = int.from_bytes(addr1.packed, byteorder="big") & int.from_bytes(addr2.packed, byteorder="big")
    return ipaddress.IPv6Address(result_int.to_bytes(16, byteorder="big"))


def bitwise_or_ipv6(addr1, addr2):
    result_int = int.from_bytes(addr1.packed, byteorder="big") | int.from_bytes(addr2.packed, byteorder="big")
    return ipaddress.IPv6Address(result_int.to_bytes(16, byteorder="big"))


def replace_ipv6_host_part(network, host_addr):
    # Compute address
    net_part = bitwise_and_ipv6(network.network_address, network.netmask)
    host_part = bitwise_and_ipv6(host_addr, network.hostmask)
    # Put together resulting IP
    return bitwise_or_ipv6(net_part, host_part)


app = Flask(__name__)


@app.errorhandler(Exception)
def handle_error(e):
    code = 500
    if isinstance(e, HTTPException):
        code = e.code
    print('Error: {}'.format(str(e)), file=sys.stderr)
    print('Route: {}'.format(request.full_path), file=sys.stderr)
    return jsonify(error=str(e)), code


@app.route('/', methods=['GET'])
def main():
    print('Main!', file=sys.stderr)
    token = request.args.get('token')
    zones = request.args.get('zone').split(',')
    ipv4 = request.args.get('ipv4')
    ipv6 = request.args.get('ipv6')
    ipv6prefix = request.args.get('ipv6prefix')
    cf = CloudFlare(token=token)

    if not token:
        print('Missing token URL parameter.', file=sys.stderr)
        return jsonify({'status': 'error', 'message': 'Missing token URL parameter.'}), 400
    if not zones or len(zones) == 0:
        print('Missing zone URL parameter.', file=sys.stderr)
        return jsonify({'status': 'error', 'message': 'Missing zone URL parameter.'}), 400
    if not ipv4 and not ipv6:
        print('Missing ipv4 or ipv6 URL parameter.', file=sys.stderr)
        return jsonify({'status': 'error', 'message': 'Missing ipv4 or ipv6 URL parameter.'}), 400

    for zone in zones:
        try:
            root_zone = '.'.join(zone.split('.')[-2:])
            zones = cf.zones.get(params={'name': root_zone})

            if not zones:
                print('Zone {} does not exist.'.format(root_zone), file=sys.stderr)
                return jsonify({'status': 'error', 'message': 'Zone {} does not exist.'.format(root_zone)}), 404

            a_record = cf.zones.dns_records.get(zones[0]['id'], params={
                'name': '{}'.format(zone), 'match': 'all', 'type': 'A'})
            aaaa_record = cf.zones.dns_records.get(zones[0]['id'], params={
                'name': '{}'.format(zone), 'match': 'all', 'type': 'AAAA'})

            if ipv4 is not None and not a_record:
                print('A record for {} does not exist.'.format(zone), file=sys.stderr)
                return jsonify({'status': 'error', 'message': 'A record for {} does not exist.'.format(zone)}), 404

            if ipv6 is not None and not aaaa_record:
                print('AAAA record for {} does not exist.'.format(zone), file=sys.stderr)
                return jsonify({'status': 'error', 'message': 'AAAA record for {} does not exist.'.format(zone)}), 404

            if ipv4 is not None and a_record[0]['content'] != ipv4:
                print('name:', a_record[0]['name'], 'type: A', 'content:', ipv4, 'proxied:', a_record[0]['proxied'], 'ttl:',
                      a_record[0]['ttl'], file=sys.stderr)
                cf.zones.dns_records.put(zones[0]['id'], a_record[0]['id'], data={
                    'name': a_record[0]['name'], 'type': 'A', 'content': ipv4, 'proxied': a_record[0]['proxied'],
                    'ttl': a_record[0]['ttl']})

            full_ipv6 = ipv6 if ipv6prefix is None else str(replace_ipv6_host_part(ipaddress.IPv6Network(ipv6prefix), ipaddress.IPv6Address(ipv6)))

            if ipv6 is not None and aaaa_record[0]['content'] != full_ipv6:
                print('name:', aaaa_record[0]['name'], 'type: AAAA', 'content:', full_ipv6, 'proxied:',
                      aaaa_record[0]['proxied'], 'ttl:', aaaa_record[0]['ttl'], file=sys.stderr)
                cf.zones.dns_records.put(zones[0]['id'], aaaa_record[0]['id'], data={
                    'name': aaaa_record[0]['name'], 'type': 'AAAA', 'content': full_ipv6, 'proxied': aaaa_record[0]['proxied'],
                    'ttl': aaaa_record[0]['ttl']})
        except exceptions.CloudFlareAPIError as e:
            print(str(e), file=sys.stderr)
            return jsonify({'status': 'error', 'message': str(e)}), 500

    print('Update successful.', file=sys.stderr)
    return jsonify({'status': 'success', 'message': 'Update successful.'}), 200


@app.route('/healthz', methods=['GET'])
def healthz():
    return jsonify({'status': 'success', 'message': 'OK'}), 200


@app.route('/favicon.ico', methods=['GET'])
def favicon():
    return send_from_directory(os.path.join(app.root_path, 'static'),
                               'favicon.ico', mimetype='image/vnd.microsoft.icon')


if __name__ == "__main__":
    import waitress

    app.secret_key = os.urandom(24)
    waitress.serve(app, host='0.0.0.0', port=80)
